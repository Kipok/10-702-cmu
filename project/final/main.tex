\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
% \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2016
%
% to avoid loading the natbib package, add option nonatbib:
% \usepackage[nonatbib]{nips_2016}

% \usepackage{nips_2016}

% to compile a camera-ready version, add the [final] option, e.g.:
\usepackage[final]{nips_2016}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{mathtools}
\usepackage{mathabx}

\title{Theoretical basis for neural networks \\training techniques}

\author{
  Igor Gitman, igitman \\ 
  \texttt{igitman@andrew.cmu.edu}
}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Pb}{\mathbb{P}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Nm}{\mathcal{N}}
\newcommand{\Ll}{\mathcal{L}}

% brackets
\newcommand{\inner}[2]{\left\langle#1,#2 \right\rangle}
\newcommand{\rbr}[1]{\left(#1\right)}
\newcommand{\sbr}[1]{\left[#1\right]}
\newcommand{\cbr}[1]{\left\{#1\right\}}
\newcommand{\nbr}[1]{\left\|#1\right\|}
\newcommand{\abr}[1]{\left|#1\right|}

% operators
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\begin{document}

\maketitle

%\begin{abstract}
%\end{abstract}

\section{Introduction}
Deep neural networks have become a state of the art methods in many machine learning areas including computer vision, speech recognition, 
natural language processing, reinforcement learning and many more. The success of deep architectures was mainly determined by the following three 
factors. 
The first factor is the availability of large amounts of data for complex problems (for instance, labeled image datasets). The second factor is the 
great increase in available computational power that modern GPUs provide. And the last factor is the development of a number of architectural
solutions and training techniques that helped to speed up the convergence of optimization methods for deep networks and ensure that they converge to a 
better local minima. Most of these techniques were first suggested as heuristics, meaning that they lacked clear theoretical basis, although being
based on some appealing intuition and working well in practice. But in the recent years there was a number of papers trying to develop an
understanding of the best working methods and find out the theory behind them. The survey of these theoretical advances is going to be the main goal
of this project.

\section{Background}
Although in theory deep neural networks are universal approximators~\cite{hornik1991approximation}, it becomes challenging to train such architectures 
well in practice. The optimization surface is highly non-convex and contains many local minimas and the best known algorithm capable of optimizing deep 
networks on big amounts of data is stochastic gradient descent (SGD) (and it's variations~\cite{duchi2011adaptive},~\cite{zeiler2012adadelta},
~\cite{kingma2014adam}) which only has sublinear rate of convergence. In addition the layered nature of such models is naturally difficult to optimize 
because of vanishing or exploding gradients problems~\cite{hochreiter1998vanishing},~\cite{hochreiter2001gradient}. And even when the convergence 
can be achieved deep models tend to overfit since the number of parameters is very big and the networks can model very complex dependencies.

The recent training techniques address all these issues making deep networks so successful in many machine learning areas. Since these techniques are
different in nature we will focus our attention on one group of methods which we call ``normalization of activations''.

The idea behind these group of methods is based on the analysis of LeCun et al~\cite{lecun1991second} which theoretically justifies that whitening (or
normalization and scaling) of inputs to each layer in the \textit{linear} neural network will speed up the convergence. The proof was based on the
analysis of the eigenvalue spectrum of the Hessian matrix. The authors showed that the proposed transformations will improve the conditioning number of 
the Hessian and thus will lead to faster convergence of the gradient descent algorithm since it will be better approximating Newton algorithm. Later,
Amari et al~\cite{amari1998natural} introduced the idea of natural gradient descent (NGD), optimization algorithm that uses Fisher information matrix
instead of the Hessian in order to account for the local curvature of the parameter space. Since NGD closely resembles Newton algorithm, it can also
be shown that improving the 
conditioning of the Fisher information matrix improves the convergence of the neural networks. In the following years there has been developed a
number of methods that implicitly or explicitly use these ideas in order to improve the learning behavior of the neural networks. The main 
instrument for modifying the Fisher information matrix in these methods is the normalization of units activations. 
The first approaches included the techniques of normalizing the data at each layer independently of the optimization being
performed~\cite{raiko2012deep},~\cite{povey2014parallel}. Later these techniques were improved by including the normalization in the gradient update 
procedure~\cite{ioffe2015batch}. The further improvements were achieved by making the normalization of the network responses independent on the 
minibatch data statistics using weights reparametrization~\cite{salimans2016weight},~\cite{arpit2016normalization}.

We will show how all these methods can be viewed as different ways to implicitly use approximate natural gradient descent (NGD) algorithm instead of 
standard SGD by improving the conditioning of the Fisher information matrix. We will start by describing NGD and
giving the intuition and theoretical results as to why this algorithm is more suited for optimizing deep neural networks. Next we will 
describe different normalization approaches, showing both intuition behind the methods and more rigorous theoretical justifications.

\section{Natural gradient and Fisher information}
Consider the standard maximum likelihood estimation problem: given $x_1,\dots,x_n \overset{iid}{\sim} q(X)$, the goal is to find an approximation of
$q(X)$ in some parametric family of distributions $p(X;\theta)$ by solving 
\begin{equation}\label{MLE}
    \theta^* = \argmax_{\theta}\sbr{\prod_{i=1}^n p(x_i;\theta)} = \argmin_{\theta}\sbr{-\sum_{i=1}^n \log p(x_i;\theta)} = \argmin_{\theta}h(\theta) 
\end{equation}
If the analytical solution is not feasible, it is still possible to solve the problem~\ref{MLE} by using an optimization method, for example gradient
descent (GD) algorithm:
\begin{equation}\label{GD}
    \theta_{k+1} = \theta_k - \alpha_k \nabla h(\theta_k)
\end{equation}
The update~\ref{GD} is a greedy approach moving the parameter vector $\theta$ in the direction of the most reduction of $h$ for a unit of reduction in
$\theta$, which corresponds to the negative gradient: 
\begin{equation}\label{GD_deriv}
    \frac{-\nabla h}{\nbr{\nabla h}} = \lim_{\epsilon \to 0}\frac{1}{\epsilon}\argmin_{d:\nbr{d}\le \epsilon}h(\theta + d)
\end{equation}
However, $\theta$ is a parametrization of a distribution $p(x;\theta)$ and the Euclidean
distance between parameter vectors $\rho\rbr{\theta_{k+1}, \theta_k}$ might not directly correspond to the distance between distributions
$\rho\rbr{p(x;\theta_{k+1}), p(x;\theta_k)}$. Indeed, consider the case of two pairs of normal distributions: the first pair $\Nm(0, 1), 
\Nm(2, 1)$ and the second pair $\Nm(0, 50), \Nm(2, 50)$. 
In the first case the two distributions are clearly different, while in the second case they are almost indistinguishable. However the Euclidean 
distance between the vector of parameters is $2$ in both cases. This leads to the problem~\ref{MLE} being poorly scaled and the convergence of the GD to
be relatively slow since it is likely to experience ``zigzagging'' behavior.

More natural metric in the space of distributions is a symmetric KL-divergence: \[\rho(p, q) = KL(p||q) + KL(q||p)\] Considering the local behavior
of this distance for the parametric family of distributions it is possible to simplify the expression:
\begin{align*}
    \rho(\theta + \delta\theta, \theta) &= \rho(p(x; \theta + \delta\theta), p(x; \theta)) = \\ &=
    \int p(x; \theta + \delta\theta)\log\frac{p(x; \theta + \delta\theta)}{p(x; \theta)}dx + 
    \int p(x; \theta)\log\frac{p(x; \theta)}{p(x; \theta + \delta\theta)}dx = \\ &=
    \int \sbr{p(x; \theta + \delta\theta) - p(x; \theta)}\sbr{\log p(x; \theta + \delta\theta) - \log p(x; \theta)}dx = \\ &=
    \int \sbr{\nabla_{\theta}p(x;\theta)^T\delta\theta + O\rbr{\nbr{\delta\theta}^2}}\sbr{\nabla_{\theta}\log p(x;\theta)^T\delta\theta +
    O\rbr{\nbr{\delta\theta}^2}}dx = \\ &=
    \int \sbr{p(x;\theta)\nabla_{\theta}\log p(x;\theta)^T\delta\theta + O\rbr{\nbr{\delta\theta}^2}}\sbr{\nabla_{\theta}\log p(x;\theta)^T\delta\theta
    + O\rbr{\nbr{\delta\theta}^2}}dx = \\ &=
    \delta\theta^T\E_{x\sim p(x;\theta)}\sbr{\nabla_{\theta}\log p(x;\theta)\nabla_{\theta}\log p(x;\theta)^T}\delta\theta + O\rbr{\nbr{\delta\theta}^3}
    \approx \delta\theta^TF(\theta)\delta\theta
\end{align*}
where $F(\theta)$ is called Fisher information matrix. Thus the ``natural'' distance metric between the parameters 
\begin{equation}\label{FisherNorm}
    \rho(\theta_{k+1}, \theta_k) = \nbr{\theta_{k+1} - \theta_k}^2_{F(\theta_k)} = \rbr{\theta_{k+1} - \theta_k}^TF(\theta_k)\rbr{\theta_{k+1} -
    \theta_k}
\end{equation}
Note that this distance is only accurate in the vicinity of the current point and $F(\theta)$ smoothly depends on the $\theta$ thus defining a
Riemannian manifold over the space of distributions. Solving analogous optimization problem to~\ref{GD_deriv} it is possible to obtain:
\[ \frac{-F^{-1}\nabla h}{\nbr{\nabla h}_F} = \lim_{\epsilon \to 0}\frac{1}{\epsilon}\argmin_{d:\nbr{d}_F \le \epsilon}h(\theta + d) \]
Thus the steepest descent direction in the space of distributions with symmetric KL-divergence as a distance metric is approximately 
$\widetilde{\nabla}h = -F^{-1}\nabla h$. This expression is called natural gradient and the update rule of NGD is given by
\[ \theta_{k+1} = \theta_k - \alpha_k F^{-1}(\theta_k)\nabla h(\theta_k) \]
Fisher information can be equivalently rewritten in the following way:
\[ F(\theta) = \E_{x\sim p(x;\theta)}\sbr{\nabla_{\theta}\log p(x;\theta)\nabla_{\theta}\log p(x;\theta)^T} = 
    -\E_{x\sim p(x;\theta)}\sbr{\nabla^2_{\theta}\log p(x;\theta)} \]
Note that if we replace $p(x;\theta)$ with the empirical distribution $\widetilde{q}(x)$ we obtain 
\[H(\theta) = \nabla^2_{\theta} h(\theta) = -\frac{1}{n}\sum_{i=1}^n\nabla^2_{\theta}\log p(x;\theta)\] which is the Hessian matrix of $h(\theta)$. 
Since $p(x;\theta)$ is an approximation of $\widetilde{q}(x)$, the Fisher matrix is an approximation of the Hessian and natural gradient descent is
thus similar to the Newton method. Indeed, NGD is independent to the reparametrization of the model and has superlinear convergence rate. Moreover, it 
works well even in the stochastic/online settings surpassing the standard stochastic gradient descent variations as was
shown by~\cite{amari1998natural},~\cite{martens2014new}. These all makes NGD an appealing choice for optimizing neural networks.

However, in the case of neural networks it is impractical to try to directly invert or even to store the Fisher information matrix in memory since its 
size is
quadratic in the number of parameters (which could be millions for modern architectures). Still, there are at least two ways to approximate the
behavior of the NGD for neural networks. The first way is to derive an approximation of the Fisher information with efficient inversion
procedure available. Some of the approaches that utilize this technique include using approximate Cholesky factorization of the inverse Fisher 
matrix~\cite{grosse2015scaling} or using Kronecker factored approximation to the Fisher matrix which is easily invertible~\cite{martens2015optimizing}.

The other way to approximate the behavior of the NGD is to use the standard SGD, but reparametrize the network or transform the units' inputs in such a 
way so that the Fisher information becomes closer to the identity and thus the usual gradient becomes closer to the natural gradient. More precisely
the convergence behavior of the gradient descent depends on the conditioning number of the Hessian (and thus the Fisher information as its
approximation)~\cite{lecun1991second}. In the next sections we will show that recently proposed normalization
techniques (\cite{ioffe2015batch},~\cite{salimans2016weight},~\cite{arpit2016normalization}) are implicitly aimed to
improve the conditioning number of the Fisher matrix and are thus theoretically justified.

\section{Natural gradient for neural networks}
We consider the problem of fitting a standard multi-layer perceptron (MLP)~\footnote{Most of the results presented in this work hold for convolution 
neural networks and for recurrent neural networks as well. But since the notation will be more complicated we use the simplest case of a standard
MLP.} to the data represented by input-output pairs 
$\{x_1, y_1\},\dots, \{x_n, y_n\}$ coming from some unknown probability distribution $q(x, y)$. In this setting the MLP defines a conditional density 
$p(y|x;\theta)$ and we assume that training is performed by maximizing the log-likelihood of the model:
\[ \theta^* \leftarrow \min_{\theta}\E_{x,y \sim \widetilde{q}}\sbr{-\log p(y|x;\theta)} = 
\min_{\theta}\frac{1}{n}\sum_{i=1}^n\sbr{-\log p(y_i|x_i;\theta)} \]
where $\widetilde{q}$ is the empirical distribution. Not all possible objective functions satisfy this assumption, but it is true for the 
standard mean squared error for the regression problem and cross-entropy for the classification problem. In the regression case $p(y|x)$ is
modeled by a normal distribution and in the classification case $p(y|x)$ is a multinomial distribution.

The MLP maps input vector $x \equiv a_0$ to the output vector $y \equiv a_L$ through a series of $L$ layers:
\begin{align*}
    &a_L = f_L(W_La_{L-1} + b_L) \\
    &\dots \\
    &a_1 = f_1(W_1a_0 + b_1)
\end{align*}
where $f_i$ is the non-linear element-wise ``activation'' function for layer $i$, $W_i \in \R^{N_i \times N_{i-1}}, b_i \in \R^{N_i}$ are the weights
and biases connecting layer $i$ to the previous layer. For the simplicity of the notation we will omit biases assuming the last coordinate of $a_i$ is 
always $1$. Thus, the whole set of the trainable parameters denoted with $\theta = \{W_1, \dots, W_L\}$. Finally, let's denote the input to the
non-linear function as $s_i = W_ia_i$.

Now let's derive an explicit expression for the Fisher information matrix for the MLP case. Following the notation of~\cite{martens2015optimizing} we 
will denote the gradient of the loss with respect to some set of parameters $v$ as $Dv \equiv D_{x,y}v \equiv -\frac{\partial\log
p(y|x;\theta)}{\partial v}$. Then

\begin{align*} 
    F(\theta) &= 
    \E_{x \sim q(x), y \sim p(y|x;\theta)}\sbr{\frac{\partial\log p(y|x;\theta)}{\partial\theta}\frac{\partial\log p(y|x;\theta)}{\partial\theta}^T} = 
    \E_{x \sim q(x), y \sim p(y|x;\theta)}\sbr{D\theta D\theta^T} \approx \\ &\approx 
    \E_{x \sim \widetilde{q}(x), y \sim p(y|x;\theta)}\sbr{D\theta D\theta^T}
\end{align*}
Since the data distribution $q(x)$ is in general unknown we approximate it with the empirical distribution $\widetilde{q}(x)$. In what follows we will
denote $\E_{x \sim \widetilde{q}(x), y \sim p(y|x;\theta)}$ as $\E$ omitting the subscript.

Note that $D\theta = [\text{vec}(DW_1)^T \dots \text{vec}(DW_L)^T]^T$, where $\text{vec}(A)$ is a vectorization operation producing a column vector 
from the columns of a matrix $A$. Then
Fisher matrix can be decomposed into $L \times L$ block matrix with $(i$-$j)$th block corresponding to the interaction between layer $i$ and layer $j$:
\begin{align*}
    F(\theta) &= \E\sbr{D\theta D\theta^T} = \E\sbr{[\text{vec}(DW_1)^T \dots \text{vec}(DW_L)^T]^T [\text{vec}(DW_1)^T \dots \text{vec}(DW_L)^T]} = \\
    &= \begin{bmatrix} \E\sbr{\text{vec}(DW_1)\text{vec}(DW_1)^T} & \dots & \E\sbr{\text{vec}(DW_1)\text{vec}(DW_L)^T} \\
                       \vdots & \ddots & \vdots \\
                       \E\sbr{\text{vec}(DW_L)\text{vec}(DW_1)^T} & \dots & \E\sbr{\text{vec}(DW_L)\text{vec}(DW_L)^T} \end{bmatrix}
\end{align*}

Using the chain rule we can expand $DW_i = (Ds_i) a_i^T$ (these are the formulas constituting backpropagation algorithm). Then one block of the
resulting matrix has the following form
\[ F(\theta)_{ij} = \E\sbr{\text{vec}(DW_i)\text{vec}(DW_j)^T} = \E\sbr{\text{vec}((Ds_i) a_i^T)\text{vec}((Ds_j) a_j^T)^T}\]
This expression can be further simplified using the following properties of the Kronecker product operation~\footnote{
The Kronecker product is defined as $A \otimes B = 
\begin{bmatrix} A_{11}B & \dots & A_{1m}B \\ \vdots & \ddots & \vdots \\ A_{n1}B & \dots & A_{nm}B \end{bmatrix}$, 
for $A \in R^{n\times m}$ and $B \in R^{p \times q}$
}
\[ \text{vec}(uv^T) = v \otimes u, (u \otimes v)^T = u^T \otimes v^T, (A \otimes B)(C \otimes D) = AC \otimes BD \]
Hence
\[ F(\theta)_{ij} = \E\sbr{\text{vec}((Ds_i) a_i^T)\text{vec}((Ds_j) a_j^T)^T} = \E\sbr{Ds_i \otimes a_i)(Ds_j \otimes a_j)^T} = 
\E\sbr{Ds_iDs_j^T \otimes a_i a_j^T} \]

Note that this expression is almost exact (at least, asymptotically, since we are using a consistent estimator of the true Fisher information matrix,
by plugging in the empirical distribution). However, all the methods that we are going to analyse use implicit (or sometimes explicit) assumption that
gradient statistics are uncorrelated with activation statistics, yielding
\[ F(\theta)_{ij} = \E\sbr{Ds_iDs_j^T \otimes a_i a_j^T} \approx \E\sbr{Ds_iDs_j^T} \otimes \E\sbr{a_i a_j^T} \]
It is likely, that this approximation doesn't become exact even asymptotically under any realistic conditions. Nevertheless, it was experimentally 
shown to be fairly accurate by~\cite{martens2015optimizing},~\cite{desjardins2015natural} both explicitly (by direct computation on a set of small 
neural networks) and implicitly (by the performance of their methods based on this assumption). Thus, in the presented analysis we will also rely on
this approximation.


\section{Single layer MLP}
Now that we derived the general formula for the Fisher information matrix, let's analyse the simplest case of the one-layer neural network. The
optimization problem in this case can be written in the following way
\[ w^* \leftarrow \min_{w}\sum_{i=1}^nL\rbr{y_i, f(w^Tx_i)} \]
The Fisher information in this case consists of only one block and has a simplified expression. Let's denote 
$g_i(y) = \frac{\partial L(y, f(w^Tx_i))}{\partial w^Tx_i}$ then
\begin{align*}
    F(w) &\approx \E\sbr{Ds_1Ds_1^T} \otimes \E\sbr{a_1 a_1^T} 
          = \E_{x \sim \widehat{q}(x), y \sim p(y|x;w)}\sbr{\rbr{\frac{\partial L\rbr{y, f(w^Tx)}}{\partial w^Tx}}^2}
          \E_{x\sim \widehat{q}(x)}\sbr{xx^T} = \\
         &= \frac{1}{n}\sum_{i=1}^n\E_{y \sim p(y|x_i;w)}\sbr{g_i(y)^2} \frac{1}{n}\sum_{i=1}^nx_ix_i^T = g(w)\Sigma
\end{align*}
%\begin{align*} 
%    F(w) &= \E\sbr{Ds_1Ds_1^T \otimes a_1 a_1^T} 
%          = \E_{x \sim \widehat{q}(x), y \sim p(y|x;w)}\sbr{\rbr{\frac{\partial L\rbr{y, f(w^Tx)}}{\partial w^Tx}}^2xx^T} = \\
%         &= \frac{1}{n}\sum_{i=1}^n\E_{y \sim p(y|x_i;w)}\sbr{g_i(y)^2} x_ix_i^T
%\end{align*}
where $g(w)$ is a scalar and $\Sigma \in \R^{d \times d}$ is a covariance matrix of the input vectors $x_i \in \R^{d}$. Therefore the conditioning 
number of the Fisher matrix 
$\kappa\rbr{F(w)} = \kappa\rbr{\Sigma} = \frac{\abr{\lambda_{\text{max}}\rbr{\Sigma}}}{\abr{\lambda_{\text{min}}\rbr{\Sigma}}}$. This gives the
theoretical justification to the whitening of the inputs, since that will result in $\Sigma = I \Rightarrow \kappa\rbr{F(w)} = 1$. Even when the
whitening is not possible, it is still reasonable to center and normalize the inputs. 
Indeed, consider simple case when all features have the same mean $m$ and variance $v$. Then as $n \to \infty$
\begin{align*} 
\Sigma &\approx 
\begin{bmatrix} \E\sbr{x_1^2} & \dots & \E\sbr{x_1x_d} \\ \vdots & \ddots & \vdots \\ \E\sbr{x_dx_1} & \dots & \E\sbr{x_d^2} \end{bmatrix} 
= \begin{bmatrix} v + m^2  & \dots & \text{cov}\sbr{x_1x_d} + m^2 \\ \vdots & \ddots & \vdots \\ \text{cov}\sbr{x_dx_1} + m^2 & \dots & 
v + m^2 \end{bmatrix} \Rightarrow \\ &\Rightarrow \kappa\rbr{\Sigma} \xrightarrow[m\to\infty]{} \infty
\end{align*}
Thus systematic bias in the input distribution mean can lead to ill-conditioned Fisher matrix and should be avoided. More rigorous proof of the fact
that scaling and centering improves the conditioning of the covariance matrix requires functional analysis of the eigenvalue spectrum and can be found
in~\cite{le1991eigenvalues}. 

It should be noted that by using similar arguments it is possible to show that input centering and normalization or whitening improves the convergence 
of \textit{linear} perceptron, even without the approximation made for the Fisher information matrix. The details can be found 
in~\cite{lecun1991second}.

\section{General MLP case}
Now we can already argue that whitening of each layers input helps in convergence of the general neural networks. Indeed, consider layer $k$. Each of
the $N_k$ output neurons can be viewed as minimizing its own local objective function with inputs $a_{k-1}$. Thus the convergence behavior of these
intermediate objective functions is determined by the conditioning of their corresponding Fisher information matrices which is improved from whitening
of the inputs as was shown in the previous section.

Note that normalization of the inputs is aimed to keep the second term in the Fisher block 
\[ F_{ii}(\theta) = \E\sbr{Ds_iDs_i^T}\otimes \E\sbr{a_ia_i^T} \]
well conditioned. However, in general (aside from the single layer network case) the first term is a matrix and thus also affects the conditioning of
the Fisher matrix. Thus it is reasonable to seek a transformation of inputs or parameters of the network so that $\E\sbr{Ds_iDs_i^T} = I$. It turns
out that in some cases to do this it is enough to require that the output to input Jacobian matrix for each layer 
$\frac{\partial a_{k+1}}{\partial a_k}$ has all singular values equal to $1$. Let's show this in the linear case, where each $f_i(x) = x$. 
In this case $s_i = a_i$ and
\begin{align*}
    Ds_i = Da_i = \frac{\partial\log p(y|x;\theta)}{\partial a_i} = 
    \frac{\partial a_{i+1}}{\partial a_i}\frac{\partial\log p(y|x;\theta)}{\partial a_{i+1}} =
    \frac{\partial a_{i+1}}{\partial a_{i}}\frac{\partial a_{i+2}}{\partial a_{i+1}} \dots 
    \frac{\partial a_L}{\partial a_{L-1}}\frac{\partial\log p(y|x;\theta)}{\partial a_{L}}
\end{align*}
where each $\frac{\partial a_{k+1}}{\partial a_k} \in R^{N_k \times N_{k+1}}$ is the Jacobian matrix for layer $k$. Then,
\begin{align*}
    \E\sbr{Ds_iDs_i^T} = 
    \E\sbr{\frac{\partial a_{i+1}}{\partial a_{i}} \dots \frac{\partial a_L}{\partial a_{L-1}}\frac{\partial\log p(y|x;\theta)}{\partial a_{L}}
    \frac{\partial\log p(y|x;\theta)}{\partial a_{L}}^T\frac{\partial a_L}{\partial a_{L-1}}^T \dots \frac{\partial a_{i+1}}{\partial a_{i}}^T}
\end{align*}
Notice that $\E\sbr{\frac{\partial\log p(y|x;\theta)}{\partial a_{L}}\frac{\partial\log p(y|x;\theta)}{\partial a_{L}}^T} = F_{LL}(W_L) \approx I$ is 
a Fisher matrix for a single layer MLP which was analyzed in the previous section\footnote{Note that we showed $F(W) = g(w)\Sigma$ in the previous 
section, however under assumption that inputs are whitened and network is linear $F(W) = I$.}. If we assume that different layers represent
different things\footnote{This assumtion is also likely to be false in practice, however under some conditions might hold approximately} and thus the
terms corresponding to different layers are independent we can show that
\begin{align*}
    \E\sbr{Ds_iDs_i^T} &\approx
    \E\sbr{\frac{\partial a_{i+1}}{\partial a_{i}} \dots \E\sbr{\frac{\partial a_L}{\partial a_{L-1}}\E\sbr{
    \frac{\partial\log p(y|x;\theta)}{\partial a_{L}}
    \frac{\partial\log p(y|x;\theta)}{\partial a_{L}}^T}\frac{\partial a_L}{\partial a_{L-1}}^T} \dots \frac{\partial a_{i+1}}{\partial a_{i}}^T} \\
    &\approx \E\sbr{\frac{\partial a_{i+1}}{\partial a_{i}} \dots \E\sbr{\frac{\partial a_L}{\partial a_{L-1}}
    \frac{\partial a_L}{\partial a_{L-1}}^T} \dots \frac{\partial a_{i+1}}{\partial a_{i}}^T} = I
\end{align*}
since $\frac{\partial a_{i+1}}{\partial a_{i}}\frac{\partial a_{i+1}}{\partial a_{i}}^T = I\ \forall i$, because 
$\frac{\partial a_{i+1}}{\partial a_{i}}$ has all singular values of $1$.

Note that this derivation is again based on the strong assumptions that are somewhat unjustified. However, even without considering Fisher information
it is possible to show that requiring Jacobians of each layer to have all singular values of $1$ helps the convergence of neural networks. Indeed,
note that in the linear case
\begin{align*}
    \frac{\partial \log p(y|x;\theta)}{\partial \text{vec}(W_i)} = 
    \frac{\partial a_i}{\partial \text{vec}(W_i)}\frac{\partial \log p(y|x;\theta)}{\partial a_i} = 
    \frac{\partial a_i}{\partial \text{vec}(W_i)}\frac{\partial a_{i+1}}{\partial a_{i}} \dots\frac{\partial a_L}{\partial a_{L-1}}
    \frac{\partial\log p(y|x;\theta)}{\partial a_{L}}
\end{align*}
But since all Jacobians have singular values of $1$ they have the property of preserving the $L_2$ norm\footnote{Matrix $J$ is said to preserve the
$L_2$ norm if $\nbr{Jx}_2 = \nbr{x}_2$. If all singular values of the $J$ are equal to $1$ then 
$\nbr{Jx}_2 = \nbr{UIV^Tx}_2 = x^TVIU^TUIV^Tx = x^Tx = \nbr{x}_2$. Here $J = UIV^T$ is a singular value decomposition.} and thus
\begin{align*}
    \nbr{\frac{\partial a_{i+1}}{\partial a_{i}} \dots\frac{\partial a_L}{\partial a_{L-1}}\frac{\partial\log p(y|x;\theta)}{\partial a_{L}}}_2 = 
    \nbr{\frac{\partial\log p(y|x;\theta)}{\partial a_{L}}}_2
\end{align*}
which is independent of the depth of the network. This intuitively justifies why the networks with this property should not experience exploding or
vanishing gradient problems~\cite{hochreiter1998vanishing},~\cite{hochreiter2001gradient}. Indeed, Saxe et al~\cite{saxe2013exact} proved that requiring
Jacobians of each layers to have all singular values close to $1$ helps network to approximately (exactly in the linear case) achieve dynamical 
isometry which makes learning efficient regardless of the number of layers.

Finally, we should note that the analysis carried out in this section only considers conditioning of the diagonal blocks of the Fisher information
matrix. However, it is also necessary to require off-diagonal blocks to be close to zero. One way to do this is to develop a reparametrization of the
network parameters that would bring off-diagonal elements closer to zero~\cite{raiko2012deep}. However, the normalization techniques analyzed in later
sections don't do this kind of transformations which limits their theoretical justification.

\section{Normalization of activations methods}
The main theoretical result that was shown in the previous sections is the following: whitening (or just scaling and centering) of the inputs to each
layer should improve the conditioning of the Fisher information matrix and thus bring the usual gradient descent algorithm closer to the natural
gradient descent algorithm which converges much faster. In addition requiring the output to input Jacobians of each layer to have all singular values 
equal to $1$ should also lead to a better conditioning of the Fisher information and should help the network to achieve approximate dynamical isometry
which means that the gradients of the last layers will be similar in magnitude to the gradients of the first layers. In the following section we will
show how the recent training techniques indeed achieve these properties and are thus theoretically justified (to some extent, because of the severe
assumptions made in the analysis).

\subsection{Naive normalization}
Whitening of each layers activations might be computationally expensive since it requires inverting a covariance matrix. However, since the simple
centering and scaling of the inputs is supposed to improve the convergence properties, why not directly normalize each layers input? That is, why not
to make the following update after each layer:
\begin{equation}\label{BN}
    \widehat{a}_{ij} \leftarrow \frac{a_{ij} - \E\sbr{a_{ij}}}{\sqrt{\V\sbr{a_{ij}}}}
\end{equation}
where the expectation and variance can be estimated from the data. As mentioned in~\cite{ioffe2015batch} the problem with this strategy is that if 
the normalization is being performed independent of the optimization,
then its effect might get canceled out. Let's illustrate it with the following example. Consider a simple neural network layer that just adds
bias to it's inputs and then its activation is centered: $a_1 = a_0 + b, \widehat{a}_1 = a_1 - \E\sbr{a_1}$. Then the effect of any gradient update on
$b$: $b^{\text{new}} \leftarrow b + \Delta b$ will lead to no update to the actual output of the network since 
\begin{align*} 
    a_1^{\text{new}} &= a_0 + b^{\text{new}} = a_0 + b + \Delta b \\
   \widehat{a}_1^{\text{new}} &= a_1^{\text{new}} - \E\sbr{a_1^{\text{new}}} = 
   a_0 + b + \Delta b - \E\sbr{a_0 + b + \Delta b} = \\ &= a_0 + b - \E\sbr{a_0 + b} = a_1 - \E\sbr{a_1} = \widehat{a}_1
\end{align*}
and thus the bias term will keep increasing with no update to the layer's output and to the objective function. So, for the normalization scheme to
be efficient it is necessary to include the normalization procedure into the optimization algorithm.

\subsection{Batch normalization}
In 2014 Ioffe et al suggested a simple, but very efficient normalization technique called batch normalization~\cite{ioffe2015batch}. Aside from the
technical issues the algorithm is conceptually simple. For each layer sample mean and variance is computed and then each layer is normalized with the
update~\ref{BN}. In order to insure that the network can approximate the same set of functions as before normalization, the authors introduce 
additional trainable linear transformation after the actual normalization. For more information about the details of the algorithm we refer to the
original paper~\cite{ioffe2015batch}.
% more details?

\subsection{Normalization propagation}
Arpit et al~\cite{arpit2016normalization} extends the idea of batch normalization further. They introduce a data independent procedure called
normalization propagation that under certain assumptions guarantees that
the activations of all layers will be normalized without the need to explicitly calculate and use data statistics. The first assumption is that input
data is generated from the Gaussian distribution $\Nm(0, I)$. Since a lot of real-world datasets follow the Gaussian distribution and training data
could be explicitly whitened, this assumption is not a very strong one. It then follows that input to the activation function $W_1a_0$ is centered since
$a_0$ is centered: $\E\sbr{W_1a_0} = 0$. In order to ensure that $\V\rbr{W_1a_0}_j = 1$ the authors follow a scheme given below. Let's use the
following notation:
$u = W_1a_0$, $\Sigma$ is the covariance matrix of $u$, $w_i$ is the $i$th row of $W_1$, diag$(\alpha)$ is the operation that produces the diagonal
matrix from the vector (placing its elements on the diagonal). Then, under the assumptions made the following proposition holds
\begin{align*}
    \min_{\alpha}\nbr{\Sigma - \text{diag}(\alpha)}_F \le \max_{i \neq j}\sbr{\frac{\abr{w_iw_j^T}}{\nbr{w_i}_2\nbr{w_j}_2}}
    \sqrt{\sum_{i\neq j}\nbr{w_i}_2^2\nbr{w_j}_2^2}
\end{align*}
with the optimal $\alpha_i^* = \nbr{W_i}_2^2$. The proof of this property is straightforward and can be found in~\cite{arpit2016normalization}. 
If the first term of the right side of the inequality is zero then the bound becomes tight and we can ensure that $u$ is normalized by dividing
corresponding components by $\nbr{W_i}_2$. The
first term is called the coherence of the rows of $W_1$ and thus the assumption made here is that the rows of $W_1$ are incoherent. Since it is
generally observed that the useful filters in neural networks are roughly incoherent~\cite{wright2010sparse} this assumption might hold approximately 
in practice. 

Thus, after dividing each component of $u$ with $\nbr{W_i}_2$ we ensured that $u$ is approximately normalized. In this case for many common
non-linearities it is possible to figure out the exact distribution of the activation $f_1(u)$. For example if $f_1(u) = \max(0, u)$ is ReLU function
then $f_1(u)$ follows the Rectified Gaussian distribution with mean $\frac{1}{\sqrt{2\pi}}$ and variance $\frac{1}{2}\rbr{1 - \frac{1}{\pi}}$. This
knowledge of the exact distribution of the first-layer activations makes it possible to renormalize them (subtract the mean and divide by the
variance) which ensures that the input to the second layer follows the Gaussian distribution $\Nm(0, I)$. By induction, following this procedure, all
the layer activations will be normalized and thus the convergence of this method should be faster then without normalization. The authors also analyse
the output to input Jacobians of each layer. By straightforward calculation they show that all singular values are approximately $1.21$ which is close
to $1$ and thus also speeds up the convergence. In the practical implementation of the normalization propagation the inputs to the activation
functions are divided by $1.21$ which should make the Jacobians to be almost identity.


\section{Conclusion}
In this project we demonstrated the available theoretical results that justify some of the recent training techniques for neural networks. We first
described the idea of natural gradient descent optimization method and showed that it can greatly improve the convergence speed of the neural networks
training. We then demonstrated different ways to bring the usual gradient descent closer to the natural gradient descent by improving the conditioning
of the Fisher information matrix. And finally we showed that the well known idea of normalizing the training data, as well as the recent techniques 
such as 
batch normalization or normalization propagation indeed bring the Fisher information closer to the identity and thus can be viewed as an 
approximation to the NGD. However, since the assumptions made for this analysis are somewhat unrealistic we can conclude that although the intuition 
is very clear, more detailed theoretical research is needed to claim that these methods are indeed theoretically justified.


\newpage
\bibliographystyle{abbrv}
\bibliography{main}

\end{document}
