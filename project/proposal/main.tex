\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
% \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2016
%
% to avoid loading the natbib package, add option nonatbib:
% \usepackage[nonatbib]{nips_2016}

% \usepackage{nips_2016}

% to compile a camera-ready version, add the [final] option, e.g.:
\usepackage[final]{nips_2016}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography

\title{Theoretical basis for neural networks regularization}

\author{
  Igor Gitman, igitman \\ 
  \texttt{igitman@andrew.cmu.edu}
}

\begin{document}

\maketitle

%\begin{abstract}
%\end{abstract}

\section{Introduction}
Deep neural networks have become a state of the art methods in many machine learning areas including computer vision, speech recognition, 
natural language processing, reinforcement learning and many more. The success of deep architectures was mainly determined by the following three 
factors. 
The first factor is the availability of large amounts of data for complex problems (for instance, labeled image datasets). The second factor is the 
great increase in available computational power that modern GPUs provide. And the last factor is the development of a number of architectural
solutions and training techniques that helped to speed up the convergence of optimization methods for deep networks and ensure that they converge to a 
better local minima. Most of these techniques were first suggested as heuristics, meaning that they lacked clear theoretical basis, although being
based on some appealing intuition and working well in practice. But in the recent years there was a number of papers trying to develop an
understanding of the best working methods and find out the theory behind them. The survey of these theoretical advances is going to be the main goal
of this project.

\section{Background}
Although in theory deep neural networks are universal approximators~\cite{hornik1991approximation}, it becomes challenging to train such architectures 
well in practice. The optimization surface is highly non-convex and contain many local minimas and the best known algorithm capable of optimizing deep 
networks on big amounts of data is stochastic gradient descent (SGD) (and it's variations~\cite{duchi2011adaptive},~\cite{zeiler2012adadelta},
~\cite{kingma2014adam}) which only has sublinear rate of convergence. In addition the layered nature of such models is naturally difficult to optimize 
because of vanishing or exploding gradients problems~\cite{hochreiter1998vanishing},~\cite{hochreiter2001gradient}. And even when the convergence 
can be achieved deep models tend to overfit since the number of parameters is very big and the networks can model very complex dependencies.

The recent training techniques address all these issues making deep networks so successful in many machine learning areas. Broadly these techniques 
can be categorized into five groups: architectural solutions, replacing SGD with its variations, smart weights initialization, activations normalization
and explicit regularization.

\subsection{Architectural solutions}
The first group regularizes the learning through architectural constraints, making the network structure suited for a particular problem. Some
examples of these techniques include using convolutional networks~\cite{lecun1998gradient} when objects are represented as images, using residual 
connections when training very deep models~\cite{he2016deep} or the choice of an activation function (for instance, using ReLU~\cite{nair2010rectified} 
instead of the sigmoid).

\subsection{Replacing SGD with its variations}
The second group addresses some of the shortcomings of SGD such as sensitivity to the scaling of the input and chosen learning rates. The variations
of the SGD proposed in the recent years include such methods as Adagrad~\cite{duchi2011adaptive}, AdaDelta~\cite{zeiler2012adadelta},
Adam~\cite{kingma2014adam} and others.

\subsection{Smart weights initialization}
The third group addresses two problems. First, it helps to prevent initially vanishing/exploding gradients by making gradients centered at $1$ and 
keeping the variance small and approximately constant between different layers. This is usually done by initializing the weights of the network from 
some fixed distribution that depends on the number of connections for a particular unit and the type of activation function
used~\cite{glorot2010understanding}. Another method of weight initialization addresses the problem of overfitting. It is done by a supervised 
pretraining of the whole network or a greedy layer-wise unsupervised pretraining which was shown to improve the generalization of the 
models~\cite{erhan2009difficulty},~\cite{saxe2013exact}.

\subsection{Activations normalization}
The fourth group speeds up the convergence of the networks by performing local whitening or normalization of the units responses. It has been long
known that the convergence speed is improved when inputs to each layer are whitened or normalized~\cite{lecun1991second},~\cite{wiesler2011convergence}.
The first approaches included the techniques of normalizing the data at each layer independently of the optimization being
performed~\cite{raiko2012deep},~\cite{povey2014parallel}. Later these techniques were improved by including the normalization in the gradient update 
procedure~\cite{ioffe2015batch}. The further improvements were achieved by making the normalization of the network responses  independent on the 
minibatch data statistics using weights reparametrization~\cite{salimans2016weight},~\cite{arpit2016normalization}.

\subsection{Explicit regularization}
And the last group of methods are utilizing the direct regularization of the model by either applying the standard $L_2$ or $L_1$ regularizations or, 
more commonly, by applying a dropout~\cite{hinton2012improving}.

The majority of theoretical results are available for the last four groups, but this project will be mainly focused on the last two. 

\section{Progress evaluation and the list of papers}
In the first part
of the report we will start by describing the general theory about why the whitening of the input improves the convergence of the network following 
the analysis in~\cite{lecun1991second}. We will then describe the theoretical limitations of the normalization if it is done independently of the 
optimization procedure as highlighted in~\cite{ioffe2015batch}. And finally we will describe most successful methods for performing activations 
normalization (i.e. Batch Normalization~\cite{ioffe2015batch}, Weight Normalization~\cite{salimans2016weight} and Normalization
Propagation~\cite{arpit2016normalization}) and their theoretical justifications. In the second part of the report we will focus on the dropout theory 
starting by describing the general intuition following~\cite{srivastava2014dropout}. Then we will give two theoretical justifications of the
dropout. The first analysis is based on the marginalizing over the noise introduced by dropout thus providing an equivalent deterministic objective 
function~\cite{wager2013dropout}. The second method is formulating a generalization of a dropout in a Bayesian framework also providing an explicit 
objective function being optimized~\cite{kingma2015variational}. And in the final part of the paper we will show the theoretical connection between 
different regularization methods. We will start by describing how dropout is related to Adagrad~\cite{wager2013dropout}. And in the end of the paper 
we will show why there might be a theoretical connection between dropout and activations normalization by considering how they both implicitly use 
Fisher information matrix to change the training procedure.

By the second milestone we will finish the first part of the paper about activation normalization and describe the main ideas of the second part about 
dropout. By the end of the project we will finish all three parts described above.

For the activation normalization part we will rely on the following papers: \cite{lecun1991second}, \cite{wiesler2011convergence}, \cite{raiko2012deep},
\cite{povey2014parallel}, \cite{ioffe2015batch}, \cite{salimans2016weight}, \cite{arpit2016normalization}.

For the dropout part we will rely on the following papers: \cite{hinton2012improving}, \cite{srivastava2014dropout}, \cite{wager2013dropout},
\cite{kingma2015variational}.

\newpage
\bibliographystyle{abbrv}
\bibliography{main}

\end{document}
